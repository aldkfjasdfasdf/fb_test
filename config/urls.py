
from django.contrib import admin
from django.urls import include, path

from rest_framework import routers

from drf_yasg import openapi
from drf_yasg.views import get_schema_view

router = routers.DefaultRouter()
# router.register(r'clients', ClientView, basename='client')

schema_view = get_schema_view(
    openapi.Info(
        title="API",
        default_version='v1',
    ),
    public=True,
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('', include('sender.urls'))
]
