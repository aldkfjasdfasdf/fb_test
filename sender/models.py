import pytz

from django.db import models
from django.core.validators import RegexValidator


class Newsletter(models.Model):
    STATUS_CHOICES = (("active", "Активная"), ("closed", "Закрытая"))

    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default="active")
    start_datetime = models.DateTimeField()
    message_text = models.TextField()
    client_filter_operator_code = models.CharField(max_length=3, blank=True, null=True)
    client_filter_tag = models.CharField(max_length=50, blank=True, null=True)
    end_datetime = models.DateTimeField()


class Client(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField(
        max_length=11,
        validators=[RegexValidator(r"^7\d{10}$", "Введите допустимый номер телефона")],
    )
    operator_code = models.CharField(max_length=10, blank=True, null=True)
    tag = models.CharField(max_length=50, blank=True, null=True)
    timezone = models.CharField(max_length=32, choices=TIMEZONES, default="UTC")


class Message(models.Model):
    STATUS_CHOICES = (
        ("pending", "Ожидает"),
        ("error", "Ошибка"),
        ("sent", "Отправлено"),
    )
    created_datetime = models.DateTimeField()
    send_status = models.CharField(max_length=50, choices=STATUS_CHOICES)
    newsletter = models.ForeignKey(
        Newsletter, on_delete=models.CASCADE, related_name="messages"
    )
    client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="messages"
    )
