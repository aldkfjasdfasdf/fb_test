from django.contrib import admin

from sender.models import Client, Newsletter, Message

# Register your models here.


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    pass


@admin.register(Newsletter)
class NewsletterAdmin(admin.ModelAdmin):
    pass


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    pass
