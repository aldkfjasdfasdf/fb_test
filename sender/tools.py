import requests
import json
from django.db.models import Q
from django.conf import settings


from .models import Newsletter, Client, Message


def get_clients(newsletter: Newsletter):
    filter_q = Q()

    if newsletter.client_filter_operator_code:
        filter_q &= Q(operator_code=newsletter.client_filter_operator_code)

    if newsletter.client_filter_tag:
        filter_q &= Q(tag=newsletter.client_filter_tag)

    clients = Client.objects.filter(filter_q) if filter_q else Client.objects.all()

    return clients


def send(message: Message) -> None:
    url = f"https://probe.fbrq.cloud/v1/send/{message.id}"
    headers = {
        "accept": "application/json",
        "Authorization": f"Bearer {settings.JWT_TOKEN}",
        "Content-Type": "application/json",
    }

    data = {
        "id": message.id,
        "phone": message.client.phone_number,
        "text": message.newsletter.message_text,
    }

    response = requests.post(url, headers=headers, data=json.dumps(data))

    if response.status_code == 200:
        message.send_status = "sent"
    else:
        message.send_status = "error"
    message.save()
