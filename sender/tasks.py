from celery import shared_task
from django.utils import timezone

from .models import Newsletter, Client, Message
from .tools import get_clients, send


@shared_task
def sender_task():
    newsletters = Newsletter.objects.filter(
        start_datetime__lt=timezone.now(),
        end_datetime__gt=timezone.now(),
        status="active",
    )

    for newsletter in newsletters:
        clients = get_clients(newsletter=newsletter)

        for client in clients:
            if not Message.objects.filter(
                newsletter=newsletter, client=client
            ).exists():
                message = Message.objects.create(
                    created_datetime=timezone.now(),
                    send_status="pending",
                    newsletter=newsletter,
                    client=client,
                )
                send(message=message)

        newsletter.status = "closed"
        newsletter.save()
