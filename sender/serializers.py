from rest_framework import serializers

from .models import Client, Message, Newsletter


class ClientSerializer(serializers.ModelSerializer):
    operator_code = serializers.CharField(read_only=True)

    class Meta:
        model = Client
        fields = ["id", "phone_number", "operator_code", "tag", "timezone"]
        read_only_fields = ["id"]


class NewsletterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Newsletter
        fields = "__all__"
