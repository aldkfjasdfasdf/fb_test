from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from django.db.models import Count, Q

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Client, Newsletter, Message
from .serializers import ClientSerializer, NewsletterSerializer


@swagger_auto_schema(
    method="post",
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            "phone_number": openapi.Schema(
                type=openapi.TYPE_STRING, example="79123456789"
            ),
            "tag": openapi.Schema(type=openapi.TYPE_STRING, example="VIP"),
            "timezone": openapi.Schema(
                type=openapi.TYPE_STRING, example="Europe/Moscow"
            ),
        },
    ),
    responses={
        201: ClientSerializer(),
        400: "Некорректные данные",
    },
)
@api_view(["POST"])
def create_client(request):
    serializer = ClientSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)


@swagger_auto_schema(
    method="put",
    manual_parameters=[
        openapi.Parameter(
            "id",
            openapi.IN_PATH,
            description="ID клиента",
            type=openapi.TYPE_INTEGER,
            required=True,
            example=1,  # примерный ID клиента
        ),
    ],
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            "phone_number": openapi.Schema(
                type=openapi.TYPE_STRING, example="79123456789"
            ),
            "tag": openapi.Schema(type=openapi.TYPE_STRING, example="VIP"),
            "timezone": openapi.Schema(
                type=openapi.TYPE_STRING, example="Europe/Moscow"
            ),
        },
    ),
    responses={
        200: ClientSerializer(),
        400: "Некорректные данные",
        404: "Клиент не найден",
    },
)
@api_view(["PUT"])
def update_client(request, pk):
    try:
        client = Client.objects.get(pk=pk)
    except Client.DoesNotExist:
        return Response(status=404)

    serializer = ClientSerializer(client, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors, status=400)


@swagger_auto_schema(
    method="delete",
    responses={
        204: "Успешное удаление",
        404: "Клиент не найден",
    },
)
@api_view(["DELETE"])
def delete_client(request, pk):
    try:
        client = Client.objects.get(pk=pk)
    except Client.DoesNotExist:
        return Response(status=404)

    client.delete()
    return Response(status=204)


class NewsletterCreateView(generics.CreateAPIView):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer


class NewsletterUpdateAPIView(generics.UpdateAPIView):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer


class NewsletterDeleteAPIView(generics.DestroyAPIView):
    queryset = Newsletter.objects.all()


class StatisticsAPIView(APIView):
    def get(self, request):
        statistics = Newsletter.objects.annotate(
            pending_count=Count("message", filter=Q(message__send_status="pending")),
            error_count=Count("message", filter=Q(message__send_status="error")),
            sent_count=Count("message", filter=Q(message__send_status="sent")),
        ).values("id", "message_text", "pending_count", "error_count", "sent_count")

        return Response(statistics)


class NewsletterStatisticsAPIView(APIView):
    def get(self, request, pk):
        try:
            newsletter = Newsletter.objects.get(pk=pk)
        except Newsletter.DoesNotExist:
            return Response({"error": "Newsletter not found"}, status=404)

        statistics = (
            Message.objects.filter(newsletter=newsletter)
            .values("send_status")
            .annotate(count=Count("send_status"))
        )

        return Response(statistics)


class ProcessNewsletterAPIView(APIView):
    def post(self, request):
        active_newsletters = Newsletter.objects.filter(status="active")

        # Обработка активных рассылок и отправка сообщений клиентам
        # for newsletter in active_newsletters:
        #     # Хз что там обрабатывать, думаю у вас какая-то другая задумка

        return Response({"message": "Рассылки обработаны и сообщения отправлены"})
