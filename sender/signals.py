from django.db.models.signals import pre_save
from django.dispatch import receiver

from .models import Client


@receiver(pre_save, sender=Client)
def set_operator_code(sender, instance, **kwargs):
    phone_number = instance.phone_number
    if phone_number and len(phone_number) >= 4:
        instance.operator_code = phone_number[1:4]
