from django.urls import path

from .views import (
    NewsletterCreateView,
    NewsletterDeleteAPIView,
    NewsletterStatisticsAPIView,
    NewsletterUpdateAPIView,
    StatisticsAPIView,
    create_client,
    update_client,
    delete_client,
)


urlpatterns = [
    path("clients/", create_client, name="create-client"),
    path("clients/<int:pk>/update/", update_client, name="update-client"),
    path("clients/<int:pk>/delete/", delete_client, name="delete-client"),
    path(
        "newsletters/create/", NewsletterCreateView.as_view(), name="newsletter-create"
    ),
    path(
        "newsletters/<int:pk>/delete/",
        NewsletterDeleteAPIView.as_view(),
        name="newsletter-delete",
    ),
    path(
        "newsletters/<int:pk>/update",
        NewsletterUpdateAPIView.as_view(),
        name="newsletter-update",
    ),
    path("statistics/", StatisticsAPIView.as_view(), name="statistics"),
    path(
        "statistics/<int:pk>/newsletter/",
        NewsletterStatisticsAPIView.as_view(),
        name="newsletter_detail_statistics",
    ),
]
