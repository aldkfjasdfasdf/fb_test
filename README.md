# Дополнительные задания

5. сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io

# 10 шагов к совершенству

## Шаг 1. Создать .env файл с данными
В репозитории находится .env.example как пример

## Шаг 2. Создать виртуального окружения
```
python3 -m venv venv
```

## Шаг 3. Активация виртуального окружения Mac, linux
```
source venv/bin/activate
```

## Шаг 4. Установка зависимостей

```
pip install -r requirements.txt
```

## Шаг 5. Выполнить миграции
```
python3 manage.py migrate
```

## Шаг 6. Создать суперюзера
```
python3 manage.py createsuperuser
```

## Шаг 7. Запустить редис (В отдельном терминале)
```
redis-server
```

## Шаг 8. Запустить celery worker (В отдельном терминале)
```
celery -A config worker --loglevel=info
```

## Шаг 9. Запустить celery beat (В отдельном терминале)
```
celery -A config beat --loglevel=info
```

## Шаг 10. Запустить проект (В отдельном терминале)
```
python3 manage.py runserver
```

# Swagger
```
http://127.0.0.1:8000/docs/
```

# API

## Создание клиента
```
POST http://127.0.0.1:8000/clients/
```

## Обновление клиента
```
PUT http://127.0.0.1:8000/clients/<int:id клиента>/update/
```

## Удаление клиента
```
DELETE http://127.0.0.1:8000/clients/<int:id клиента>/delete/
```

## Создание рассылки

```
POST http://127.0.0.1:8000/newsletters/create/
```
## Удаление рассылки
```
POST http://127.0.0.1:8000/newsletters/<int:id рассылки>/delete/
```

## Обновление рассылки
```
PUT, PATCH http://127.0.0.1:8000/newsletters/<int:id рассылки>/update/
```

## Статистика

```
GET http://127.0.0.1:8000/statistics/
```

## Статистика рассылки

```
GET http://127.0.0.1:8000/statistics/<int:id рассылки>/newsletter/
```

## Обработка активных рассылок

Не совсем понял что требуется. 
Думаю в плане тестового лучше не тратить общее время с вопросами на эту тему.

